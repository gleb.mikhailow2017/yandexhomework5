using UnityEngine;
using UnityEngine.UI;

public class CoinsRenderer
{
    private Text _render;
    private Animator _animator;

    public CoinsRenderer(Text render, Animator animator)
    {
        _render = render;
        _animator = animator;
    }

    public void ChangeRenderState(int _amount)
    {
        _render.text = $"Coins: {_amount}";
        _animator.SetTrigger("OnPickupCoin");
    }
}