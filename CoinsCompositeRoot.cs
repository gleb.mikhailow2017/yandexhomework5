using UnityEngine;
using UnityEngine.UI;

public class CoinsCompositeRoot
{
    [SerializedField] private Text _render;
    [SerializedField] private Animator _animator;
    [SerializedField] private CoinsCounter _coinsCounter;

    private CoinsRenderer _coinsRenderer;

    public CoinsRenderer CoinsRenderer => _coinsRender;
    public CoinsCounter CoinsCounter => _coinsCounter;

    public void Compose()
    {
        _coinsRender = new CoinsRenderer(_render,_animator);
    }

    private void OnEnable()
    {
        _coinsCounter._coinsRender += _coinsRenderer.ChangeRenderState;
    }

    private void OnDisable()
    {
        _coinsCounter._coinsRender -= _coinsRenderer.ChangeRenderState;
    }
}