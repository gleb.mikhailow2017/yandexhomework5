using UnityEngine;

public class CoinsCounter : MonoBehaviour
{
    private int _amount;

    private Action<int> _coinsRender

    private void Awake()
    {
        _amount = PlayerPrefs.GetInt("Coins");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Contains("Coin"))
            OnPickupCoin();
    }

    public void OnPickupCoin()
    {
        _amount++;
        _coinsRender?.Invoke(_amount);
        PlayerPrefs.SetInt("Coins", _amount);
    }

    public bool TryDiscard(int price)
    {
        if (_amount < price)
            return false;

        _amount -= price;
        _coinsRender?.Invoke(_amount);
        PlayerPrefs.SetInt("Coins", _amount);

        return true;
    }
}
